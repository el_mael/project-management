# project-management
small python code for managing project details
note: receipt not yet working
# How to use
Clear the tables and start fresh with no values
```
clear_table()
```
Add projects from a `JSON` file. Make sure the `JSON` file is in the same folder as `main.py`.
`JSON` file should have format as below:
```
[
  [231030, 'Project Name', 'Client', 'Contact Person', Contract Value], ...
]
```
```
batch_add_proj()
```
### Contacts
```
search_contact(contact)
```
```
add_contact(contact, data01=None, data02=None)
# contact is a string variable
# data01 and data02 can either be a mobile number or an email address
```
```
edit_contact(contact_id, delete=False, data=None)
# contact is an int variable, id for the contact person
# to delete the contact, delete=True
# if delete=False, this will edit contact email or number depending on data value
```
### Client
```
search_client(client)
```
```
add_client(client, contact=None)
# both inputs are string variables
# contact is added to the database if it is not existing
```
```
edit_client(client_id, delete=False, contact=None)
# client_id is an int variable refers to id assigned to client
# change delete=True to delete the client
# contact is changed in the Clients table for specified client if delete=False
# contact is added to the database if it is not existing
```
### Project
```
search_proj(project_name)
```
```
add_proj(project_name, client_name, project_id=None, contact=None, contract=0)
# project_id will be generated if not provided
# contract is the contract value
```
```
edit_proj(project_id, delete=True, command=None)
# if delete=False, items in the Projects table will be edited depending on command
# editing contact should have format contact@{contact_name}@email/number:{data}
# editing client should have format client@{new_client}
# editing contract value should have format contract@{new_contract_value}
# editing project name should have format name@{new_project_name}
```
```
show_all_proj()
```
=======

>>>>>>> 551c071174e180227992fc2e1283a95064de094b
