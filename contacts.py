import sqlite3

conn=sqlite3.connect('db.sqlite')

def lookfor_id(table, column, value, ignore=True):
	global conn
	cur = conn.cursor()
	if ignore: sqlstr = f'insert or ignore into {table} ({column}) values (\"{value}\")'
	else: sqlstr = f'insert into {table} ({column}) values (\"{value}\")'
	cur.execute(sqlstr)

	sqlstr = f'select (id) from {table} where {column}=\"{value}\"'
	cur.execute(sqlstr)
	return cur.fetchone()[0]

def search_contact(contact):
	global conn
	cur = conn.cursor()

	sqlstr = f'select * from Contacts where Contact like \"%{contact}%\"'
	q = cur.execute(sqlstr)
	entries = list()
	count=0
	for each in q:
		entries.insert(count, {'id': each[0], 'name': each[1], 'email': each[2], 'number': each[3]})  
		count=count+1
	return entries

def add_contact(contact, data01=None, data02=None):
	global conn
	cur = conn.cursor()
	email=None
	number=None

	if data01 and '@' in data01: email=data01
	elif data01 and '@' not in data01: number=data01
	if data02 and '@' in data02: email=data02
	elif data02 and '@' not in data02: number=data02

	try:
		contact_id = lookfor_id('Contacts', 'Contact', contact, False)
	except:
		print('Contact already exists...')
		quit()
	if email is not None: cur.execute('update Contacts set email=? where id=?', (email, contact_id))
	if number is not None: cur.execute('update Contacts set number=? where id=?', (number, contact_id))
	cur.execute('select * from Contacts where id=?', (contact_id,))
	q = cur.fetchone()
	entry = {'id': q[0], 'name': q[1], 'email': q[2], 'number': q[3]} 

	conn.commit()
	return entry

def edit_contact(contact_id, delete=False, data=None):
	global conn
	cur = conn.cursor()
	email_edit=False
	number_edit=False

	if delete: cur.execute('delete from Contacts where id=?', (contact_id,)); conn.commit(); quit()
	if '@' in data: email = data; email_edit=True
	else: number = data; number_edit=True
	
	if email_edit: sqlstr = f'update Contacts set email=\"{email}\" where id={contact_id}' 
	if number_edit: sqlstr = f'update Contacts set number={number} where id={contact_id}'
	cur.execute(sqlstr)
	cur.execute('select * from Contacts where id=?', (contact_id,))
	q = cur.fetchone()
	entry = {'id': q[0], 'name': q[1], 'email': q[2], 'number': q[3]}

	conn.commit()
	return entry 


