import sqlite3
import re
import os
from datetime import datetime as dt
import json
import contacts, clients, projects


#connect to database. quit if not found
try:
	conn = sqlite3.connect('db.sqlite')
except:
	print('database not found..')
	quit()

def clear():
	try:
		os.system('cls')
	except:
		os.system('clear')

#clear table in database
def clear_table():
	global conn
	cur = conn.cursor()
	cur.executescript('''
		drop table if exists Projects;
		drop table if exists Clients;
		drop table if exists Contacts;
		drop table if exists Receipts;

		create table Contacts(
			id integer not null primary key autoincrement unique,
			Contact text not null unique,
			email text,
			number text
		);
		
		create table Clients(
			id integer not null primary key autoincrement unique,
			Client text not null unique,
			contact_id integer
		);

		create table Projects(
			id integer not null primary key autoincrement unique,
			Project text not null,
			client_id integer,
			contact_id integer,
			Contract integer not null
		);

		create table Receipts(
			id integer not null primary key autoincrement unique,
			receipt text,
			cost integer not null
		)

	''')
	conn.commit()

def lookfor_id(table, column, value, ignore=True):
	global conn
	cur = conn.cursor()
	if ignore: sqlstr = f'insert or ignore into {table} ({column}) values (\"{value}\")'
	else: sqlstr = f'insert into {table} ({column}) values (\"{value}\")'
	cur.execute(sqlstr)

	sqlstr = f'select (id) from {table} where {column}=\"{value}\"'
	cur.execute(sqlstr)
	return cur.fetchone()[0]

def batch_add_proj():
	global conn
	cur = conn.cursor()
	
	#get data from file
	file = open('projects.json')
	data = json.loads(file.read())
	
	for each in data:
		project_id = each[0]
		project_name = each[1]
		client = each[2]
		contact = each[3]
		contract = each[4]

		contact_id = lookfor_id('Contacts', 'Contact', contact) 
		
		cur.execute('insert or ignore into Clients (Client, contact_id) values (?, ?)', (client,contact_id))
		cur.execute('select (id) from Clients where Client=?', (client,))
		client_id = cur.fetchone()[0]

		cur.execute('insert or ignore into Projects (id, Project, client_id, contact_id, Contract) values (?,?,?,?,?)', (project_id, project_name, client_id, contact_id, contract))

		conn.commit()
