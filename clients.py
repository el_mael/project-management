import sqlite3

conn = sqlite3.connect('db.sqlite')

def lookfor_id(table, column, value, ignore=True):
	global conn
	cur = conn.cursor()
	if ignore: sqlstr = f'insert or ignore into {table} ({column}) values (\"{value}\")'
	else: sqlstr = f'insert into {table} ({column}) values (\"{value}\")'
	cur.execute(sqlstr)

	sqlstr = f'select (id) from {table} where {column}=\"{value}\"'
	cur.execute(sqlstr)
	return cur.fetchone()[0]

def search_client(client):
	global conn
	cur = conn.cursor()

	sqlstr = f'select Clients.id, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number from Clients join Contacts on Clients.contact_id=Contacts.id where Clients.Client like \'%{client}%\''
	entries = list()
	count = 0
	for each in cur.execute(sqlstr):
		entries.insert(count, {'id': each[0], 'client': each[1], 'contact': each[2], 'email': each[3], 'number': each[4]})
		count=count+1
	return entries

def add_client(client, contact=None):
	global conn
	cur = conn.cursor()

	if contact is not None: contact_id = lookfor_id('Contacts', 'Contact', contact)
	client_id = lookfor_id('Clients', 'Client', client)

	try:
		cur.execute('update Clients set contact_id=? where id=?', (contact_id, client_id))
	except:
		pass
	
	entry = search_client(client)
	conn.commit()
	return entry

def edit_client(client_id, delete=False, contact=None):
	global conn
	cur = conn.cursor()

	if delete: cur.execute('delete from Clients where id=?', (client_id,)); conn.commit(); quit()
	
	if contact is not None: contact_id = lookfor_id('Contacts', 'Contact', contact)

	try:
		cur.execute('update Clients set contact_id=? where id=?', (contact_id, client_id))
	except:
		pass
	
	sqlstr = f'select Clients.id, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number from Clients join Contacts on Clients.contact_id=Contacts.id where Clients.id={client_id}'
	cur.execute(sqlstr)
	q = cur.fetchone()
	entry = {'id': q[0], 'client': q[1], 'contact': q[2], 'email': q[3], 'number': q[4]}
	conn.commit()

	return entry
	
def show_all():
	global conn
	cur = conn.cursor()

	sqlstr = "select Clients.id, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number from Clients join Contacts on Clients.contact_id=Contacts.id"
	count = 0
	entries = list()
	for each in cur.execute(sqlstr):
		entries.insert(count, {'id': each[0], 'client': each[1], 'contact': each[2], 'email': each[3], 'number': each[4]})
		count=count+1

	return entries
