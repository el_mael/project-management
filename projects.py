import sqlite3
from datetime import datetime as dt
import re

conn = sqlite3.connect('db.sqlite')

def lookfor_id(table, column, value, ignore=True):
	global conn
	cur = conn.cursor()
	if ignore: sqlstr = f'insert or ignore into {table} ({column}) values (\"{value}\")'
	else: sqlstr = f'insert into {table} ({column}) values (\"{value}\")'
	cur.execute(sqlstr)

	sqlstr = f'select (id) from {table} where {column}=\"{value}\"'
	cur.execute(sqlstr)
	return cur.fetchone()[0]


def search_proj(project_name, shortcuts=False):
	global conn
	cur = conn.cursor()
	
	#sqlstr = f'select id, Project, Contract from Projects where Project like \"%{project_name}%\"'
	sqlstr = f'select Projects.id, Projects.Project, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number, Projects.Contract from Projects join Clients on Projects.client_id=Clients.id join Contacts on Projects.contact_id=Contacts.id where Projects.Project like \"%{project_name}%\"'
	count = 0
	entries = list()
	for each in cur.execute(sqlstr):
		entries.insert(count, {'id': each[0], 'project': each[1], 'client': each[2], 'contact': each[3], 'email': each[4], 'number': each[5], 'contract': each[6]})
		count=count+1

	if shortcuts:
		for each in entries:
			print(each['id'], each['project'], each['client'], each['contact'], each['email'], each['number'], each['contract'])
		quit()
	return entries

def add_proj(project_name, client_name, project_id=None, contact=None, contract=None, shortcuts=False):
	global conn
	cur = conn.cursor()
	
	if project_id is None:
		xDate = str(dt.today())
		project_id = re.match('[0-9-]+', xDate)[0].replace('-','')[2:]
	
	client_id = lookfor_id('Clients', 'Client', client_name)
	if contract is not None: cur.execute('update Projects set Contract=? where id=?', (contract,project_id))

	try:
		cur.execute('insert into Projects (id, Project, client_id, contract) values (?,?,?,?)', (project_id, project_name, client_id, contract))
	except:
		return print('project id already existed')
		
	
	if contact is not None: 
		contact_id = lookfor_id('Contacts','Contact',contact)
		cur.execute('update Projects set contact_id=? where id=?', (contact_id,project_id))
	
	sqlstr = f'select Projects.id, Projects.Project, Clients.Client, Projects.Contract from Projects join Clients on Projects.client_id=Clients.id where Projects.id={project_id}'
	cur.execute(sqlstr)
	q = cur.fetchone()
	entry = {'id': q[0], 'project': q[1], 'client': q[2], 'contract': q[3]}
	conn.commit()

	if shortcuts:
		return print(entry['id'], entry['project'], entry['client'], f"{each['contract']:,}")
		
	return entry

def edit_proj(project_id, delete=False, command=None, shortcuts=False):
	global conn
	cur = conn.cursor()

	if delete: cur.execute('delete from Projects where id=?', (project_id,)); conn.commit(); quit()
	
	# format should be contact@{contact_name}@email/number:{data}
	if 'contact' in command.split('@')[0]:
		contact = command.split('@')[1]
		contact_id = lookfor_id('Contacts', 'Contact', contact)
		if 'email' in command.split('@')[2]: 
			email=command.split('@')[2].split(':')[1] 
			cur.execute('update Contacts set email=? where id=?', (email, contact_id))
		if 'number' in command.split('@')[2]: 
			number = command.split('@')[2].split(':')[1]
			cur.execute('update Contacts set number=? where id=?', (number, contact_id))
		cur.execute('update Projects set contact_id=? where id=?',(contact_id, project_id))
		#cur.execute('select Projects.id, Projects.Project, Contacts.Contact from Projects join Contacts on Projects.contact_id=Contacts.id where Projects.id=?', (project_id,))
		cur.execute('select Projects.id, Projects.Project, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number, Projects.Contract from Projects join Clients on Projects.client_id=Clients.id join Contacts on Projects.contact_id=Contacts.id where Projects.id=?', (project_id,))
		fetch = cur.fetchone()
		entry = {'id': fetch[0], 'project': fetch[1], 'client': fetch[2], 'contact': fetch[3], 'email': fetch[4], 'number': fetch[5], 'contract': fetch[6]}
		if shortcuts:
			return print(entry['id'], entry['project'], entry['client'], entry['contact'], entry['email'], entry['number'], f"{each['contract']:,}")
			
		return entry
	
	# format should be client@{new_client}
	if 'client' in command.split('@')[0]:
		client = command.split('@')[1]
		client_id = lookfor_id('Clients', 'Client', client)
		cur.execute('update Projects set client_id=? where id=?', (client_id, project_id))
		#cur.execute('select Projects.id, Projects.Project, Clients.Client from Projects join Clients on Projects.client_id=Clients.id where Projects.id=?', (project_id,))
		#fetch = cur.fetchone()
		#entry = {'id': fetch[0], 'project': fetch[1], 'client': fetch[2]}
		#return entry
		cur.execute('select Projects.id, Projects.Project, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number, Projects.Contract from Projects join Clients on Projects.client_id=Clients.id join Contacts on Projects.contact_id=Contacts.id where Projects.id=?', (project_id,))
		fetch = cur.fetchone()
		entry = {'id': fetch[0], 'project': fetch[1], 'client': fetch[2], 'contact': fetch[3], 'email': fetch[4], 'number': fetch[5], 'contract': fetch[6]}
		if shortcuts:
			return print(entry['id'], entry['project'], entry['client'], entry['contact'], entry['email'], entry['number'], f"{each['contract']:,}")
			
		return entry

	if 'contract' in command.split('@')[0]:
		contract = command.split('@')[1]
		cur.execute('update Projects set Contract=? where id=?', (contract, project_id))
		#cur.execute('select id, Project, Contract from Projects where id=?', (project_id,))
		#fetch = cur.fetchone()
		#entry = {'id': fetch[0], 'project': fetch[1], 'contract': fetch[2]}
		#return entry
		cur.execute('select Projects.id, Projects.Project, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number, Projects.Contract from Projects join Clients on Projects.client_id=Clients.id join Contacts on Projects.contact_id=Contacts.id where Projects.id=?', (project_id,))
		fetch = cur.fetchone()
		entry = {'id': fetch[0], 'project': fetch[1], 'client': fetch[2], 'contact': fetch[3], 'email': fetch[4], 'number': fetch[5], 'contract': fetch[6]}
		if shortcuts:
			return print(entry['id'], entry['project'], entry['client'], entry['contact'], entry['email'], entry['number'], f"{each['contract']:,}")
			
		return entry

	if 'name' in command.split('@')[0]:
		name = command.split('@')[1]
		cur.execute('update Projects set Project=? where id=?', (name, project_id))
		#cur.execute('select id, Project, Contract from Projects where id=?', (project_id,))
		#fetch = cur.fetchone()
		#entry = {'id': fetch[0], 'project': fetch[1], 'contract': fetch[2]}
		#return entry
		cur.execute('select Projects.id, Projects.Project, Clients.Client, Contacts.Contact, Contacts.email, Contacts.number, Projects.Contract from Projects join Clients on Projects.client_id=Clients.id join Contacts on Projects.contact_id=Contacts.id where Projects.id=?', (project_id,))
		fetch = cur.fetchone()
		entry = {'id': fetch[0], 'project': fetch[1], 'client': fetch[2], 'contact': fetch[3], 'email': fetch[4], 'number': fetch[5], 'contract': fetch[6]}
		if shortcuts:
			return print(entry['id'], entry['project'], entry['client'], entry['contact'], entry['email'], entry['number'], f"{each['contract']:,}")
			
		return entry

	conn.commit()

def show_all(shortcuts=False):
	global conn
	cur = conn.cursor()

	sqlstr = 'select Projects.id, Projects.Project, Clients.Client, Projects.Contract from Projects join Clients on Projects.client_id=Clients.id'
	entries = list()
	count = 0

	for each in cur.execute(sqlstr):
		entries.insert(count, {'id': each[0], 'project': each[1], 'client': each[2], 'contract': each[3]})
		count=count+1

	if shortcuts: 
		for each in entries:
			print(each['id'], each['project'], each['client'], f"{each['contract']:,}")
		quit()
	return entries
